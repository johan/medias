# Changelog

## Unreleased

### Added

- Installable en tant que package Composer

### Changed

- Externalisation de la lib [james-heinrich/getid3](https://packagist.org/packages/james-heinrich/getid3)
- Appel de l'archiviste via `use Spip\Archiver\SpipArchiver`
- Compatible SPIP 5.0.0-dev

### Fixed

- #4957 Accessibilité et UX des boutons de gestion des documents
- #4952 HTML mal formé sur modeles/document_desc.html
- #4931 Normaliser `autoriser_document_voir()` pour retourner toujours un booleen
- #4941 Compléter le jeu d’icones pour certains types de documents
- #4946 Affichage des puces statut des documents dans la médiathèque
- #4944 Ne pas appliquer 2 fois les traitements typo sur les titres, descriptifs et crédits de document
- HTML5: Retrait des `CDATA` et `text/javascript` dans les balises `<script>`
